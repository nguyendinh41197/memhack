package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.Unit;

import java.util.List;

public interface CallbackUnit extends CallbackError{
    void getListUnit(List<Unit> unitList);
}
