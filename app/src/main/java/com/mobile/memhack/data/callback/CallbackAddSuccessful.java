package com.mobile.memhack.data.callback;

public interface CallbackAddSuccessful extends CallbackError {
    void callbackSuccessful(boolean isSuccessful);
}
