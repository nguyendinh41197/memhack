package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.Mem;
import com.mobile.memhack.model.User;

import java.util.List;

public interface CallbackMem extends CallbackError {
    void getListMem(List<Mem> memList, User user);
}
