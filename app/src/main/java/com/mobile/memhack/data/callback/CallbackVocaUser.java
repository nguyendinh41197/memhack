package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.VocaUser;

import java.util.List;

public interface CallbackVocaUser extends CallbackError {
    void getVocaUser(List<VocaUser> vocaUsers);
}
