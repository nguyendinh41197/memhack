package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.CourseUser;

import java.util.List;

public interface CallbackCourseUser extends CallbackError {
    void getCourseUser(List<CourseUser> courseUsers);
}
