package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.Voca;

import java.util.List;

public interface CallbackVoca extends CallbackError {
    void getListVoca(List<Voca> vocaList);
}
