package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.UnitUser;

import java.util.List;

public interface CallbackUnitUser extends CallbackError {
    void getUnitUser(List<UnitUser> unitUsers);
}
