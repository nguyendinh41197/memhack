package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.Course;

import java.util.List;

public interface CallbackCourse extends CallbackError {
    void getAllCourse(List<Course> allCourse);
}
