package com.mobile.memhack.data.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackCourse;
import com.mobile.memhack.data.callback.CallbackCourseUser;
import com.mobile.memhack.data.callback.CallbackMem;
import com.mobile.memhack.data.callback.CallbackQuestion;
import com.mobile.memhack.data.callback.CallbackUnit;
import com.mobile.memhack.data.callback.CallbackUnitUser;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.data.callback.CallbackVoca;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.model.Course;
import com.mobile.memhack.model.CourseUser;
import com.mobile.memhack.model.Mem;
import com.mobile.memhack.model.Question;
import com.mobile.memhack.model.Unit;
import com.mobile.memhack.model.UnitUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.Voca;
import com.mobile.memhack.model.VocaUser;

import java.util.ArrayList;
import java.util.List;

public class ApiHelper implements IApiHelper {
    private AppManager mManager;
    private Context mContext;
    private DatabaseReference mDataRefCourse;
    private DatabaseReference mDataRefCourseUser;
    private DatabaseReference mDataRefUnit;
    private DatabaseReference mDataRefUnitUser;
    private DatabaseReference mDataRefVoca;
    private DatabaseReference mDataRefVocaUser;
    private DatabaseReference mDataRefMem;
    private DatabaseReference mDataRefUser;

    public ApiHelper(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void callAllCourse(final CallbackCourse callback) {
        final List<Course> mAllCourse = new ArrayList<>();
        mDataRefCourse = FirebaseDatabase.getInstance().getReference().child("Course");
        mDataRefCourse.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Course course = snapshot.getValue(Course.class);
                    mAllCourse.add(course);
                }
                callback.getAllCourse(mAllCourse);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void callUnit(final long CourseId, final CallbackUnit callback) {
        final List<Unit> mUnitList = new ArrayList<>();
        mDataRefUnit = FirebaseDatabase.getInstance().getReference().child("Unit");
        mDataRefUnit.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Unit unit = snapshot.getValue(Unit.class);
                    if (CourseId == unit.getCourseId()) {
                        mUnitList.add(unit);
                    }
                }
                callback.getListUnit(mUnitList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void callVoca(final long UnitId, final CallbackVoca callback) {
        final List<Voca> mVocaList = new ArrayList<>();
        mDataRefVoca = FirebaseDatabase.getInstance().getReference().child("Voca");
        mDataRefVoca.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Voca voca = snapshot.getValue(Voca.class);
                    if (voca.getUnitId() == UnitId) {
                        mVocaList.add(voca);
                    }
                }
                callback.getListVoca(mVocaList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }


    @Override
    public void callUser(final CallbackUser callback) {
        final List<User> mUserList = new ArrayList<>();
        mDataRefUser = FirebaseDatabase.getInstance().getReference().child("User");
        mDataRefUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    mUserList.add(user);
                }
                callback.getListUser(mUserList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void callMem(User user, long idVoca, CallbackMem callback) {
        final List<Mem> mMemList = new ArrayList<>();
        List<Mem> mems = new ArrayList<>();
        mDataRefMem = FirebaseDatabase.getInstance().getReference().child("Mem");
        mDataRefMem.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Mem mem = snapshot.getValue(Mem.class);
                    if (mem.getIdVoca() == idVoca) {
                        mems.add(mem);
                    }
                }

                for (int i = 0; i < mems.size(); i++) {
                    if (mems.get(i).getIdUser().equals(user.getUserId())) {
                        mMemList.add(mems.get(i));
                    }
                }

                callback.getListMem(mMemList, user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean addCourseUser(Course course, String idUser) {
        mDataRefCourseUser = FirebaseDatabase.getInstance().getReference().child("CourseUser");
        CourseUser courseUser = new CourseUser(idUser,
                course.getCourseId(),
                course.getCourseName(),
                course.getCourseNumWord(),
                course.getCourseThumb(),
                0);
        return mDataRefCourseUser.push().setValue(courseUser).isSuccessful();
    }


    @Override
    public boolean addUnitUser(Unit unit, String idUser) {
        mDataRefUnitUser = FirebaseDatabase.getInstance().getReference().child("UnitUser");

        UnitUser unitUser = new UnitUser(idUser,
                unit.getCourseId(),
                unit.getUnitId(),
                unit.getUnitName(),
                unit.getUnitNumWord(),
                0);
        return mDataRefUnitUser.push().setValue(unitUser).isSuccessful();
    }

    @Override
    public boolean addVocaUser(Voca voca, String idUser) {
        mDataRefVocaUser = FirebaseDatabase.getInstance().getReference().child("VocaUser");

        VocaUser vocaUser = new VocaUser(voca.getUnitId(),
                voca.getVocaAudio(),
                voca.getVocaId(),
                voca.getVocaMean(),
                0,
                voca.getVocaWord(),
                idUser,
                "false");
        return mDataRefVocaUser.push().setValue(vocaUser).isSuccessful();
    }


    @Override
    public void callCourseUser(String idUser, CallbackCourseUser callback) {
        final List<CourseUser> mCourseUsers = new ArrayList<>();
        mDataRefCourseUser = FirebaseDatabase.getInstance().getReference().child("CourseUser");
        mDataRefCourseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    CourseUser course = snapshot.getValue(CourseUser.class);
                    course.setId(snapshot.getKey());
                    if (idUser.equals(course.getIdUser())) {
                        mCourseUsers.add(course);
                    }
                }
                callback.getCourseUser(mCourseUsers);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void callUnitUser(String idUser, long CourseId, CallbackUnitUser callback) {
        final List<UnitUser> mUnitList = new ArrayList<>();
        mDataRefUnitUser = FirebaseDatabase.getInstance().getReference().child("UnitUser");
        mDataRefUnitUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    UnitUser unit = snapshot.getValue(UnitUser.class);
                    unit.setId(snapshot.getKey());
                    if (CourseId == unit.getCourseId() && idUser.equals(unit.getIdUser())) {
                        mUnitList.add(unit);
                    }
                }
                callback.getUnitUser(mUnitList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void callVocaUser(String idUser, long UnitId, CallbackVocaUser callback) {
        final List<VocaUser> mVocaUsers = new ArrayList<>();
        mDataRefVocaUser = FirebaseDatabase.getInstance().getReference().child("VocaUser");
        mDataRefVocaUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    VocaUser vocaUser = snapshot.getValue(VocaUser.class);
                    vocaUser.setId(snapshot.getKey());
                    if (UnitId == vocaUser.getUnitId() && idUser.equals(vocaUser.getIdUser())) {
                        mVocaUsers.add(vocaUser);
                    }
                }
                callback.getVocaUser(mVocaUsers);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void callQuestion(long idUnit, CallbackQuestion callback) {
        final List<Question> questionList = new ArrayList<>();
        mDataRefVocaUser = FirebaseDatabase.getInstance().getReference().child("Question");
        mDataRefVocaUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Question question = snapshot.getValue(Question.class);
                    if (idUnit == question.getIdUnit()) {
                        questionList.add(question);
                    }
                }
                callback.getListQuestion(questionList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.notifyError(databaseError.getMessage());
            }
        });
    }

}
