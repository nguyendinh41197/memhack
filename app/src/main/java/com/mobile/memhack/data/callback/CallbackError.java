package com.mobile.memhack.data.callback;

public interface CallbackError {
    void notifyError(String message);
}
