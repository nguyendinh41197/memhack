package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.User;

import java.util.List;

public interface CallbackUser extends CallbackError{
    void getListUser(List<User> userList);
}
