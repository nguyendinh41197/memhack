package com.mobile.memhack.data;

import android.app.Application;

import com.mobile.memhack.data.callback.CallbackCourse;
import com.mobile.memhack.data.callback.CallbackCourseUser;
import com.mobile.memhack.data.callback.CallbackMem;
import com.mobile.memhack.data.callback.CallbackQuestion;
import com.mobile.memhack.data.callback.CallbackUnit;
import com.mobile.memhack.data.callback.CallbackUnitUser;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.data.callback.CallbackVoca;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.data.network.ApiHelper;
import com.mobile.memhack.data.network.IApiHelper;
import com.mobile.memhack.model.Course;
import com.mobile.memhack.model.Unit;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.Voca;

public class AppManager implements IAppManager {
    private static AppManager instance;

    public static AppManager getInstance(){
        return instance;
    }

    private IApiHelper iApiHelper;

    public AppManager(Application application) {
        this.iApiHelper = new ApiHelper(application);
    }

    public static void from(Application application) {
        if (instance == null) {
            instance = new AppManager(application);
        }
    }

    @Override
    public void callAllCourse(CallbackCourse callback) {
        iApiHelper.callAllCourse(callback);
    }

    @Override
    public void callUnit(long CourseId, CallbackUnit callback) {
        iApiHelper.callUnit(CourseId, callback);
    }

    @Override
    public void callVoca(long UnitId, CallbackVoca callback) {
        iApiHelper.callVoca(UnitId, callback);
    }

    @Override
    public void callUser(CallbackUser callback) {
        iApiHelper.callUser(callback);
    }

    @Override
    public void callMem(User user, long idVoca, CallbackMem callback) {
        iApiHelper.callMem(user, idVoca, callback);
    }

    @Override
    public boolean addCourseUser(Course course, String idUser) {
        return iApiHelper.addCourseUser(course, idUser);
    }

    @Override
    public boolean addUnitUser(Unit unit, String idUser) {
        return iApiHelper.addUnitUser(unit, idUser);
    }

    @Override
    public boolean addVocaUser(Voca voca, String idUser) {
        return iApiHelper.addVocaUser(voca, idUser);
    }


    @Override
    public void callCourseUser(String idUser, CallbackCourseUser callback) {
        iApiHelper.callCourseUser(idUser, callback);
    }

    @Override
    public void callUnitUser(String idUser, long CourseId, CallbackUnitUser callback) {
        iApiHelper.callUnitUser(idUser, CourseId, callback);
    }

    @Override
    public void callVocaUser(String idUser, long UnitId, CallbackVocaUser callback) {
        iApiHelper.callVocaUser(idUser, UnitId, callback);
    }

    @Override
    public void callQuestion(long idUnit, CallbackQuestion callback) {
        iApiHelper.callQuestion(idUnit, callback);
    }
}
