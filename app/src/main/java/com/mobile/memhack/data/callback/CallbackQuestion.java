package com.mobile.memhack.data.callback;

import com.mobile.memhack.model.Question;

import java.util.List;

public interface CallbackQuestion extends CallbackError {
    void getListQuestion(List<Question> questionList);
}
