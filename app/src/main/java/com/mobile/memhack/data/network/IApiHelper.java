package com.mobile.memhack.data.network;

import com.mobile.memhack.data.callback.CallbackCourse;
import com.mobile.memhack.data.callback.CallbackCourseUser;
import com.mobile.memhack.data.callback.CallbackMem;
import com.mobile.memhack.data.callback.CallbackQuestion;
import com.mobile.memhack.data.callback.CallbackUnit;
import com.mobile.memhack.data.callback.CallbackUnitUser;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.data.callback.CallbackVoca;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.model.Course;
import com.mobile.memhack.model.Unit;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.Voca;

public interface IApiHelper {
    void callAllCourse(CallbackCourse callback);
    void callUnit(long CourseId, CallbackUnit callback);
    void callVoca(long UnitId, CallbackVoca callback);
    void callUser(CallbackUser callback);
    void callMem(User user, long idVoca, CallbackMem callback);
    boolean addCourseUser(Course course, String idUser);
    boolean addUnitUser(Unit unit, String idUser);
    boolean addVocaUser(Voca voca, String idUser);
    void callCourseUser(String idUser, CallbackCourseUser callback);
    void callUnitUser(String idUser, long CourseId, CallbackUnitUser callback);
    void callVocaUser(String idUser, long UnitId, CallbackVocaUser callback);
    void callQuestion(long idUnit, CallbackQuestion callback);
}
