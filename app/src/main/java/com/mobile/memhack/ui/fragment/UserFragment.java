package com.mobile.memhack.ui.fragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.fordata.AdapterListFriend;
import com.mobile.memhack.adapter.fordata.AdapterListFriendSearch;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.ui.activity.SettingActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserFragment extends BaseFragment {
    @BindView(R.id.rl_list_friend)
    RecyclerView mRlFriend;
    @BindView(R.id.avata)
    CircleImageView mAvata;
    @BindView(R.id.txt_name_user)
    TextView mTxtUser;
    @BindView(R.id.txt_score)
    TextView mTxtScore;
    @BindView(R.id.txt_number)
    TextView mTxtNumber;
    @BindView(R.id.setting)
    LinearLayout mImgSetting;
    @BindView(R.id.ic_add_friend)
    ImageView mIcAddFriend;

    //data
    private String mIdUser;
    private AppManager mManager;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;
    private List<User> listRank;
    private List<User> listSearchUser;
    private List<User> listAllUser;

    private User myUser;

    private AdapterListFriend mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    //context
    private Context mContext;

    private AdapterListFriendSearch.CallbackChooseAddFriend chooseAddFriend = new AdapterListFriendSearch.CallbackChooseAddFriend() {
        @Override
        public void chooseAddFriend(User user) {
            //add friend
            if (isCheckFriend(user)) {
                Toast.makeText(mContext, "Người dùng đã có trong danh sách bạn bè của bạn!", Toast.LENGTH_SHORT).show();
            } else {
                String listFriend = myUser.getListFriends() + user.getUserId() + ",";
                try {
                    mUserRef.child("ListFriends").setValue(listFriend);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Toast.makeText(mContext, "Kết bạn thành công!", Toast.LENGTH_SHORT).show();
                mManager.callUser(callbackUser);
            }

        }
    };

    private boolean isCheckFriend(User user) {
        boolean is = false;
        for (int i = 0; i < listRank.size(); i++) {
            if (user.getUserId().equals(listRank.get(i).getUserId())) {
                is = true;
                break;
            }
        }
        return is;
    }

    private CallbackUser callbackUser = new CallbackUser() {
        @Override
        public void getListUser(List<User> userList) {
            listRank.clear();
            listAllUser = userList;
            for (int i = 0; i < userList.size(); i++) {
                if (mIdUser.equals(userList.get(i).getUserId())) {

                    myUser = userList.get(i);
                    mTxtUser.setText(myUser.getUsername());
                    mTxtScore.setText(myUser.getUserScore());
                    mTxtNumber.setText(myUser.getNumberOfLearned());

                    StorageReference storageReference = FirebaseStorage.getInstance().getReference();
                    final StorageReference imgRef = storageReference.child("avatar/" + myUser.getUserAvatar());
                    imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Glide.with(getActivity().getApplicationContext()).load(uri).into(mAvata);
                        }
                    });
                }
            }


            String[] friends = myUser.getListFriends().split(",");

            // sap xep thu tu theo diem
            for (int i = 0; i < userList.size(); i++) {
                for (String w : friends) {
                    if (w.equals(userList.get(i).getUserId())) {
                        listRank.add(userList.get(i));
                    }
                }
            }

            Collections.sort(listRank, new Comparator<User>() {
                @Override
                public int compare(User user1, User user2) {
                    if (Integer.parseInt(user1.getUserScore()) < Integer.parseInt(user2.getUserScore())) {
                        return 1;
                    } else {
                        if (Integer.parseInt(user1.getUserScore()) == Integer.parseInt(user2.getUserScore())) {
                            return 0;
                        } else {
                            return -1;
                        }
                    }
                }
            });

            mRlFriend.setLayoutManager(layoutManager);
            mAdapter = new AdapterListFriend(listRank, mContext);
            mRlFriend.setAdapter(mAdapter);
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        mContext = container.getContext();
        initFrag(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @OnClick({R.id.ic_add_friend, R.id.setting})
    public void onCreateClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ic_add_friend:
                final Dialog dialog = new Dialog(mContext);
                LayoutInflater inflater = LayoutInflater.from(mContext);
                View content = inflater.inflate(R.layout.custom_dialog_add_friend, null);
                dialog.setContentView(content);
                RecyclerView mRvSearch = content.findViewById(R.id.rv_add_friend);
                Button mBtnOK = content.findViewById(R.id.btn_ok);
                EditText mEdtSearch = content.findViewById(R.id.edt_search);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
                mRvSearch.setLayoutManager(layoutManager);
                mEdtSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        listSearchUser.clear();
                        for (User u : listAllUser) {
                            if (s.toString().length() == 0)listSearchUser.clear();
                            else if (u.getUsername().contains(s.toString())) listSearchUser.add(u);
                        }
                        AdapterListFriendSearch adapter = new AdapterListFriendSearch(mContext, listSearchUser, chooseAddFriend);
                        mRvSearch.setAdapter(adapter);

                    }
                });

                mBtnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.setting:
                SettingActivity.start(mContext, myUser);
                break;

        }
    }

    @Override
    public void setup(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        listSearchUser = new ArrayList<>();
        listAllUser = new ArrayList<>();
        layoutManager = new LinearLayoutManager(mContext);
        listRank = new ArrayList<>();
        if (mAuth.getCurrentUser() != null) {
            mUserRef = FirebaseDatabase.getInstance().getReference().child("User").child(mAuth.getCurrentUser().getUid());
            mIdUser = mUserRef.getKey();
        }
        mManager.callUser(callbackUser);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }
}
