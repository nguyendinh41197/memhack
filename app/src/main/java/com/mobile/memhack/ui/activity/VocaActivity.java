package com.mobile.memhack.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.forview.AdapterViewVoca;
import com.mobile.memhack.custom.CustomViewPager;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.VocaUser;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VocaActivity extends BaseActivity {

    @BindView(R.id.voca_toolbar)
    Toolbar mToolbar;
    CustomViewPager mViewPage;

    //data
    private AppManager mManager;
    private AdapterViewVoca mAdapterView;
    private List<VocaUser> mListVoca;
    private VocaUser voca;
    private int position;
    private User user;

    private FirebaseAuth mAuth;


    public static void start(Context context, VocaUser voca, int position) {
        Intent starter = new Intent(context, VocaActivity.class);
        starter.putExtra(BundleUtils.KEY_SEND_VOCA, voca);
        starter.putExtra(BundleUtils.KEY_SEND_POS_UNIT, position);
        context.startActivity(starter);
    }

    private CallbackVocaUser callbackVoca = new CallbackVocaUser() {
        @Override
        public void getVocaUser(List<VocaUser> vocaUsers) {
            mListVoca.clear();
            mListVoca = vocaUsers;
            String title = position + 1 + "/" + vocaUsers.size();
            getSupportActionBar().setTitle(title);

            mAdapterView = new AdapterViewVoca(mListVoca, VocaActivity.this, position, user);
            mViewPage.setAdapter(mAdapterView);
            mViewPage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {
                    String title = i + 1 + "/" + mListVoca.size();
                    getSupportActionBar().setTitle(title);
                }

                @Override
                public void onPageSelected(int i) {
                }

                @Override
                public void onPageScrollStateChanged(int i) {
                }
            });
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voca);
        initAct();
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        setToolbar();
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mListVoca = new ArrayList<>();
        mViewPage = (CustomViewPager)findViewById(R.id.vp_voca);
        mViewPage.setPagingEnabled(true);
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String json = mPrefs.getString(BundleUtils.KEY_USER, "N/A");
        Gson gson = new Gson();
        user = gson.fromJson(json, User.class);
        voca = (VocaUser) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_VOCA);

        position = getIntent().getIntExtra(BundleUtils.KEY_SEND_POS_UNIT, 0);
        mManager.callVocaUser(user.getUserId(), voca.getUnitId(), callbackVoca);
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }
}
