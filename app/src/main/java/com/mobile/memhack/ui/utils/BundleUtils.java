package com.mobile.memhack.ui.utils;

public class BundleUtils {
    public static final String PREF_COURSE_CHOOSE = "PREF_COURSE_CHOOSE";
    public static final String KEY_SEND_UNIT = "SEND_UNIT";
    public static final String KEY_SEND_ID_COURSE = "SEND_ID_COURSE";
    public static final String KEY_SEND_ID_UNIT = "SEND_ID_UNIT";
    public static final String KEY_SEND_POS_UNIT = "SEND_POS_UNIT";
    public static final String KEY_SEND_VOCA = "SEND_VOCA";
    public static final String KEY_SEND_USER = "SEND_USER";
    public static final String KEY_USER = "USER";
    public static final String KEY_SEND_COURSE = "SEND_COURSE";
    public static final String KEY_RANDOM_QUESTION = "RANDOM_QUESTION";
    public static final int REQUEST_CODE_GALLERY = 1;

}
