package com.mobile.memhack.ui.fragment;

import android.view.View;

public interface IBaseFragment {
    void initFrag(View view);

    void setup(View view);

    void getData();

    void updateUI();

    void releaseData();
}
