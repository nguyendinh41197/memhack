package com.mobile.memhack.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.fordata.AdapterListVoca;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.model.CourseUser;
import com.mobile.memhack.model.UnitUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.VocaUser;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UnitActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView mRlVoca;
    @BindView(R.id.unit_toolbar)
    Toolbar toolbar;
    @BindView(R.id.numberword)
    TextView num_word;
    @BindView(R.id.progressbar)
    ProgressBar mProgressbar;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.ll_learn)
    LinearLayout mBtnLearn;
    //data
    private AppManager mManager;
    private UnitUser unit;
    private AdapterListVoca mAdapterVoca;
    private RecyclerView.LayoutManager layoutManager;
    private String title;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;
    private User user;

    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    private SharedPreferences mPrefCourse;
    private SharedPreferences.Editor mEditorCurse;


    public static void start(Context context, UnitUser unit, CourseUser course) {
        Intent starter = new Intent(context, UnitActivity.class);
        starter.putExtra(BundleUtils.KEY_SEND_UNIT, unit);
        starter.putExtra(BundleUtils.KEY_SEND_COURSE, course);
        context.startActivity(starter);
    }

    private AdapterListVoca.CallbackChooseVoca callbackVoca = new AdapterListVoca.CallbackChooseVoca() {
        @Override
        public void chooseVoca(VocaUser voca, int i) {
            VocaActivity.start(getApplicationContext(), voca, i);
        }
    };

    private CallbackVocaUser callback = new CallbackVocaUser() {
        @Override
        public void getVocaUser(List<VocaUser> vocaUsers) {
            mRlVoca.setLayoutManager(layoutManager);
            mAdapterVoca = new AdapterListVoca(getApplicationContext(), vocaUsers, callbackVoca);
            mRlVoca.setAdapter(mAdapterVoca);
            mAdapterVoca.notifyDataSetChanged();
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit);
        initAct();
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        setToolbar();
        setView();
    }

    private void setView() {
        num_word.setText(unit.getUnitNumMindWord() + "/" + unit.getUnitNumWord());
        mProgressbar.setMax((int) unit.getUnitNumWord());
        mProgressbar.setProgress((int) unit.getUnitNumMindWord());
        mTitle.setText(unit.getUnitName());
        mBtnLearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LearnActivity.start(UnitActivity.this, unit);
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        layoutManager = new LinearLayoutManager(getApplicationContext());
        unit = (UnitUser) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_UNIT);
        CourseUser courseUser = (CourseUser) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_COURSE);
        mPref = this.getSharedPreferences(BundleUtils.KEY_SEND_ID_UNIT, Activity.MODE_PRIVATE);
        mEditor = mPref.edit();
        mEditor.putString(BundleUtils.KEY_SEND_ID_UNIT, unit.getId());
        mEditor.apply();
        mPrefCourse = getSharedPreferences(BundleUtils.KEY_SEND_ID_COURSE, Activity.MODE_PRIVATE);
        mEditorCurse = mPrefCourse.edit();
        mEditorCurse.putString(BundleUtils.KEY_SEND_ID_COURSE, courseUser.getId());
        mEditorCurse.apply();
        title = courseUser.getCourseName();
        mAuth = FirebaseAuth.getInstance();
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String json = mPrefs.getString(BundleUtils.KEY_USER, "N/A");
        Gson gson = new Gson();
        user = gson.fromJson(json, User.class);
        mManager.callVocaUser(user.getUserId(), unit.getUnitId(), callback);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }
}
