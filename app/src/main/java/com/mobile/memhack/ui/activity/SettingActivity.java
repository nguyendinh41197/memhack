package com.mobile.memhack.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mobile.memhack.R;
import com.mobile.memhack.model.User;
import com.mobile.memhack.ui.utils.BundleUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {
    @BindView(R.id.setting_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.ll_edit_user)
    LinearLayout mLlEditUser;
    @BindView(R.id.ll_logout)
    LinearLayout mLlLogout;
    private User user;
    private FirebaseUser userFir;


    public static void start(Context context, User user) {
        Intent starter = new Intent(context, SettingActivity.class);
        starter.putExtra(BundleUtils.KEY_SEND_USER, user);
        context.startActivity(starter);
    }

    private void sendToStart() {
        Intent startIntent = new Intent(SettingActivity.this, StartActivity.class);
        startActivity(startIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initAct();
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        setToolbar();
    }

    @OnClick({R.id.ll_edit_user, R.id.ll_logout, R.id.ll_edit_pass})
    public void onCreateClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_edit_user:
                EditUserActivity.start(this);
                break;
            case R.id.ll_edit_pass:
                final Dialog dialog = new Dialog(this);
                LayoutInflater inflater = LayoutInflater.from(this);
                View content = inflater.inflate(R.layout.custom_dialog_change_pass, null);
                dialog.setContentView(content);
                EditText edtPassOld = content.findViewById(R.id.edit_pass_old);
                EditText edtPassNew = content.findViewById(R.id.edit_pass_new);
                Button btnOK = content.findViewById(R.id.btn_ok);

                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtPassOld.getText().toString().length() == 0 || edtPassNew.getText().toString().length() == 0) {
                            Toast.makeText(SettingActivity.this, "Vui lòng điền đầy đủ thông tin và mật khẩu!", Toast.LENGTH_SHORT).show();
                        } else {
                            AuthCredential credential = EmailAuthProvider.getCredential(userFir.getEmail(), edtPassOld.getText().toString());
                            userFir.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        userFir.updatePassword(edtPassNew.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    // thay doi thanh cong
                                                    dialog.dismiss();
                                                    Toast.makeText(SettingActivity.this, "Đổi mật khẩu thành công!!", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    // thay doi that bai
                                                    Toast.makeText(SettingActivity.this, "Đổi mật khẩu không thành công!", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } else {
                                        // mat khau cu sai
                                        Toast.makeText(SettingActivity.this, "Mật khẩu cũ không chính xác!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    }
                });

                dialog.show();
                break;
            case R.id.ll_logout:
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                FirebaseAuth.getInstance().signOut();
                                sendToStart();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                dialog.dismiss();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Bạn có muốn đăng xuất?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                break;

        }
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Cài đặt");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void getData() {
        user = (User) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_USER);
        userFir = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }
}
