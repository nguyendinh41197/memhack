package com.mobile.memhack.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.forview.AdapterViewLearn;
import com.mobile.memhack.custom.CustomViewPager;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackQuestion;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.model.Question;
import com.mobile.memhack.model.UnitUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.VocaUser;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LearnActivity extends BaseActivity {
    @BindView(R.id.ic_exit)
    ImageView mIcExit;
    @BindView(R.id.txt_score)
    TextView mTxtScorse;
    CustomViewPager mViewPager;

    //data
    private AppManager mManager;
    private List<VocaUser> listVoca;
    private List<Question> listQuestion;
    private UnitUser unitUser;
    private AdapterViewLearn adapterLearn;
    private User user;

    public static void start(Context context, UnitUser unitUser) {
        Intent starter = new Intent(context, LearnActivity.class);
        starter.putExtra(BundleUtils.KEY_SEND_UNIT, unitUser);
        context.startActivity(starter);
    }

    private CallbackUser callbackUser = new CallbackUser() {
        @Override
        public void getListUser(List<User> userList) {
            for (int i = 0; i < userList.size(); i++) {
                if ((user.getUserId()).equals(userList.get(i).getUserId())) {
                    user = userList.get(i);
                }
            }

            mTxtScorse.setText(user.getUserScore());
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private AdapterViewLearn.CallbackNextpage nextpage = new AdapterViewLearn.CallbackNextpage() {
        @Override
        public void nextPage() {
            if (mViewPager.getCurrentItem() +1 == listQuestion.size()) {
                LearnActivity.super.onBackPressed();
            } else {
                mViewPager.setCurrentItem(getItem(+1), true);
                mManager.callUser(callbackUser);
            }

        }
    };

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    private CallbackQuestion callbackQuestion = new CallbackQuestion() {
        @Override
        public void getListQuestion(List<Question> questionList) {
            listQuestion = new ArrayList<>();
            listQuestion.clear();
            List<Question> listMind = new ArrayList<>();
            List<Question> listNoMind = new ArrayList<>();
            listQuestion.clear();
            listMind.clear();
            listNoMind.clear();

            for (int i = 0; i < listVoca.size(); i++) {
                for (int j = 0; j < questionList.size(); j++) {
                    if (listVoca.get(i).getVocaId() == questionList.get(j).getIdVoca()) {
                        if (listVoca.get(i).getIsBoolde().equals("true")) {
                            listMind.add(questionList.get(j));

                        } else {
                            listNoMind.add(questionList.get(j));
                        }
                    }
                }
            }


            for (int i = 0; i < listMind.size(); i++) {
                listQuestion.add(listMind.get(i));
            }

            for (int i = 0; i < listNoMind.size(); i++) {
                listQuestion.add(listNoMind.get(i));
            }

            for (int i = 0; i < listNoMind.size(); i++) {
                listQuestion.add(listNoMind.get(i));
            }

            Collections.shuffle(listQuestion);

            adapterLearn = new AdapterViewLearn(listQuestion, listVoca, LearnActivity.this, user, nextpage);
            mViewPager.setOffscreenPageLimit(listQuestion.size() - 1);
            mViewPager.setAdapter(adapterLearn);
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private CallbackVocaUser callbackUnit = new CallbackVocaUser() {
        @Override
        public void getVocaUser(List<VocaUser> vocaUsers) {
            listVoca.clear();
            listVoca = vocaUsers;
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);
        initAct();
    }

    @OnClick(R.id.ic_exit)
    public void onCreateClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ic_exit:
                dialogBack();
                break;
        }
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        mTxtScorse.setText(user.getUserScore());
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        listVoca = new ArrayList<>();
        mViewPager = (CustomViewPager) findViewById(R.id.vp_learn);
        mViewPager.setPagingEnabled(false);
        unitUser = (UnitUser) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_UNIT);
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String json = mPrefs.getString(BundleUtils.KEY_USER, "N/A");
        Gson gson = new Gson();
        user = gson.fromJson(json, User.class);
        mManager.callVocaUser(user.getUserId(), unitUser.getUnitId(), callbackUnit);
        mManager.callQuestion(unitUser.getUnitId(), callbackQuestion);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            dialogBack();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }

    private void dialogBack() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        SharedPreferences check = getSharedPreferences(BundleUtils.KEY_RANDOM_QUESTION, MODE_PRIVATE);
                        SharedPreferences.Editor editor = check.edit();
                        editor.putString(BundleUtils.KEY_RANDOM_QUESTION, "");
                        editor.commit();
                        LearnActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Bạn có chắc chắn muốn thoát?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
