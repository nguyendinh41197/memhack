package com.mobile.memhack.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.memhack.R;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackCourse;
import com.mobile.memhack.data.callback.CallbackUnit;
import com.mobile.memhack.data.callback.CallbackVoca;
import com.mobile.memhack.model.Course;
import com.mobile.memhack.model.Unit;
import com.mobile.memhack.model.Voca;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.register_input_name)
    TextInputLayout inputName;
    @BindView(R.id.register_input_email)
    TextInputLayout inputEmail;
    @BindView(R.id.register_input_password)
    TextInputLayout inputPassword;
    @BindView(R.id.register_button_create)
    Button btnCreate;
    @BindView(R.id.register_toolbar)
    Toolbar toolbar;

    //data
    private AppManager mManager;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ProgressDialog mRegDialog;
    private List<Course> courseList;
    private String ID;

    private CallbackCourse callAllCourse = new CallbackCourse() {
        @Override
        public void getAllCourse(List<Course> allCourse) {
            courseList = allCourse;
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private CallbackUnit callUnit = new CallbackUnit() {
        @Override
        public void getListUnit(List<Unit> unitList) {
            for (int i = 0; i < unitList.size(); i++) {
                mManager.addUnitUser(unitList.get(i), ID);
                mManager.callVoca(unitList.get(i).getUnitId(), callVoca);
            }
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private CallbackVoca callVoca = new CallbackVoca() {
        @Override
        public void getListVoca(List<Voca> vocaList) {
            for (int i = 0; i < vocaList.size(); i++) {
                mManager.addVocaUser(vocaList.get(i), ID);
            }
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initAct();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Create Account");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.register_button_create)
    public void onCreateClick(View view) {
        String displayName = inputName.getEditText().getText().toString();
        String email = inputEmail.getEditText().getText().toString();
        String password = inputPassword.getEditText().getText().toString();
        if (!TextUtils.isEmpty(displayName) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {
            mRegDialog.setTitle("Registering User");
            mRegDialog.setMessage("Please wait");
            mRegDialog.setCanceledOnTouchOutside(false);
            mRegDialog.show();

            registerUser(displayName, email, password);
        }
    }

    private void registerUser(final String displayName, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                            String uid = current_user.getUid();
                            ID = uid;
                            mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(uid);

                            HashMap<String, String> userMap = new HashMap<>();
                            userMap.put("UserId", uid);
                            userMap.put("Username", displayName);
                            userMap.put("UserAvatar", "default");
                            userMap.put("NumberOfLearned", "0");
                            userMap.put("UserScore", "0");
                            userMap.put("ListFriends", uid + ",");

                            mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        mManager.addCourseUser(courseList.get(0), uid);
                                        mManager.callUnit(courseList.get(0).getCourseId(), callUnit);

                                        mManager.addCourseUser(courseList.get(1), uid);
                                        mManager.callUnit(courseList.get(1).getCourseId(), callUnit);

                                        mRegDialog.dismiss();

                                        Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mainIntent);
                                        finish();

                                    }

                                }
                            });

                        } else {
                            mRegDialog.hide();
                            Toast.makeText(RegisterActivity.this, "Cannot Sign in. Please check the from and try again",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    @Override
    public void setup() {
        ButterKnife.bind(this);
        mRegDialog = new ProgressDialog(this);
        setToolbar();
    }

    @Override
    public void getData() {
        courseList = new ArrayList<>();
        mManager = AppManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mManager.callAllCourse(callAllCourse);
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }
}
