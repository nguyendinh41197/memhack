package com.mobile.memhack.ui.activity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.MimeTypeFilter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobile.memhack.R;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditUserActivity extends BaseActivity {
    @BindView(R.id.edit_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.rl_apply)
    RelativeLayout mRlApply;
    @BindView(R.id.rl_edit_avata)
    RelativeLayout mEditAvata;
    @BindView(R.id.avata)
    CircleImageView mAvata;
    @BindView(R.id.edt_name)
    EditText mName;
    @BindView(R.id.txt_email)
    TextView mEmail;
    @BindView(R.id.edit_pass)
    EditText mPass;

    private FirebaseUser userFir;
    private User user;
    private Uri uriImg;
    private DatabaseReference mUserRef;
    private StorageReference mReference;
    private AppManager mManager;

    public static void start(Context context) {
        Intent starter = new Intent(context, EditUserActivity.class);
        context.startActivity(starter);
    }

    private CallbackUser callbackUser = new CallbackUser() {
        @Override
        public void getListUser(List<User> userList) {
            for (int i = 0; i < userList.size(); i++) {
                if ((userFir.getUid()).equals(userList.get(i).getUserId())) {
                    user = userList.get(i);
                }
            }
            setView();
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        initAct();
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        setToolbar();
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        userFir = FirebaseAuth.getInstance().getCurrentUser();
        mUserRef = FirebaseDatabase.getInstance().getReference().child("User").child(userFir.getUid());
        mReference = FirebaseStorage.getInstance().getReference("avatar/");
        mManager.callUser(callbackUser);
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Sửa thông tin cá nhân");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setView() {
        final StorageReference imgRef = mReference.child(user.getUserAvatar());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(getApplicationContext()).load(uri).into(mAvata);
            }
        });

        mName.setText(user.getUsername());
        mEmail.setText(userFir.getEmail());
    }


    @OnClick({R.id.rl_apply, R.id.rl_edit_avata})
    public void onCreateClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.rl_apply:
                if (mPass.getText().toString().length() == 0 || mName.getText().toString().length() == 0) {
                    Toast.makeText(EditUserActivity.this, "Vui lòng điền đầy đủ thông tin và mật khẩu!", Toast.LENGTH_SHORT).show();
                } else {
                    AuthCredential credential = EmailAuthProvider.getCredential(userFir.getEmail(), mPass.getText().toString());
                    userFir.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                mUserRef.child("Username").setValue(mName.getText().toString());
                                uploadAvata();
                                finish();
                                Toast.makeText(EditUserActivity.this, "Đổi thông tin thành công!!", Toast.LENGTH_SHORT).show();
                            } else {
                                // mat khau cu sai
                                Toast.makeText(EditUserActivity.this, "Mật khẩu không chính xác!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                break;

            case R.id.rl_edit_avata:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
                break;
        }
    }

    private String getExtension(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

    private void uploadAvata() {
        StorageReference Ref = mReference.child(System.currentTimeMillis() + "." + getExtension(uriImg));
        Ref.putFile(uriImg).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mUserRef.child("UserAvatar").setValue(Ref.getName());
                if (!user.getUserAvatar().equals("default")){
                    deleteAvataOld();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    private void deleteAvataOld(){
        StorageReference imgRef = mReference.child(user.getUserAvatar());
        imgRef.delete();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uriImg = data.getData();
            getContentResolver().takePersistableUriPermission(uriImg, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            mAvata.setImageURI(uriImg);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }
}
