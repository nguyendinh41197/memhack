package com.mobile.memhack.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.mobile.memhack.R;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.ui.fragment.HomeFragment;
import com.mobile.memhack.ui.fragment.UserFragment;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {
    @BindView(R.id.content_view)
    FrameLayout mFrView;
    @BindView(R.id.navigation)
    BottomNavigationView mNavigationView;

    //data
    private AppManager mManager;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;

    private CallbackUser callUser = new CallbackUser() {
        @Override
        public void getListUser(List<User> userList) {
            for (int i = 0; i < userList.size(); i++){
                if ((mUserRef.getKey()).equals(userList.get(i).getUserId())){
                    User user = userList.get(i);
                    SharedPreferences.Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(user, User.class);
                    prefsEditor.putString(BundleUtils.KEY_USER, json);
                    prefsEditor.apply();
                }
            }
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    replaceFragment(new HomeFragment());
                    return true;
                case R.id.navigation_user:
                    replaceFragment(new UserFragment());
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAct();
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        this.setDefaultFragment(new HomeFragment());
        mNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void setDefaultFragment(Fragment defaultFragment) {
        this.replaceFragment(defaultFragment);
    }

    public void replaceFragment(Fragment destFragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_view, destFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            mUserRef = FirebaseDatabase.getInstance().getReference().child("User").child(mAuth.getCurrentUser().getUid());
            mManager.callUser(callUser);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            sendToStart();
        }
    }

    @Override
    public void updateUI() {
    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }

    private void sendToStart() {
        Intent startIntent = new Intent(MainActivity.this, StartActivity.class);
        startActivity(startIntent);
        finish();
    }
}
