package com.mobile.memhack.ui.activity;

public interface IBaseActivity {
    void initAct();

    void setup();

    void getData();

    void updateUI();

    void releaseData();

    void attachFragment();

    void detachFragment();
}
