package com.mobile.memhack.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.model.Mem;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.VocaUser;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMemActivity extends BaseActivity {
    @BindView(R.id.addmem_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.txt_word_en)
    TextView mTxtWordEn;
    @BindView(R.id.txt_word_vn)
    TextView mTxtWordVn;
    @BindView(R.id.ic_audio)
    ImageView mIcAudio;
    @BindView(R.id.edt_add_mem)
    EditText mEdtMem;
    @BindView(R.id.btn_add_mem)
    Button mBtnMem;
    //data
    private AppManager mManager;
    private VocaUser vocaUser;
    private User user;
    private DatabaseReference mDataRef;
    private String url;

    public static void start(Context context, VocaUser vocaUser, User user) {
        Intent starter = new Intent(context, AddMemActivity.class);
        starter.putExtra(BundleUtils.KEY_SEND_VOCA, vocaUser);
        starter.putExtra(BundleUtils.KEY_SEND_USER, user);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mem);
        initAct();
    }

    @Override
    public void setup() {
        ButterKnife.bind(this);
        setToolbar();
        setTextVoca();
    }

    private void setTextVoca() {
        mTxtWordEn.setText(vocaUser.getVocaWord());
        mTxtWordVn.setText(vocaUser.getVocaMean());
        mIcAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                mIcAudio.setBackground(getResources().getDrawable(R.drawable.ic_un_audio_24dp));
                try {
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                    mediaPlayer.prepare();
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mIcAudio.setBackground(getResources().getDrawable(R.drawable.ic_audio_24dp));
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mEdtMem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    mBtnMem.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                } else {
                    mBtnMem.setBackgroundColor(getResources().getColor(R.color.colorADD));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mBtnMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mem mem = new Mem(vocaUser.getVocaId(), mEdtMem.getText().toString(), user.getUserId(), user.getUsername());
                if (mem.getContent().length() != 0) {
                    Log.e("TAG", "add");
                    mDataRef = FirebaseDatabase.getInstance().getReference().child("Mem");
                    mDataRef.push().setValue(mem);
                    Toast.makeText(AddMemActivity.this, "Thêm gợi ý nhớ từ thành công!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(user.getUsername());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void getData() {
        mManager = AppManager.getInstance();
        vocaUser = (VocaUser) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_VOCA);
        user = (User) getIntent().getSerializableExtra(BundleUtils.KEY_SEND_USER);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final StorageReference imgRef = storageReference.child("audio/" + vocaUser.getVocaAudio());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                url = uri.toString();
            }
        });
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }

    @Override
    public void attachFragment() {

    }

    @Override
    public void detachFragment() {

    }
}
