package com.mobile.memhack.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.fordata.AdapterListAllCourse;
import com.mobile.memhack.adapter.fordata.AdapterListCourse;
import com.mobile.memhack.adapter.fordata.AdapterListUnit;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackCourse;
import com.mobile.memhack.data.callback.CallbackCourseUser;
import com.mobile.memhack.data.callback.CallbackUnit;
import com.mobile.memhack.data.callback.CallbackUnitUser;
import com.mobile.memhack.data.callback.CallbackVoca;
import com.mobile.memhack.model.Course;
import com.mobile.memhack.model.CourseUser;
import com.mobile.memhack.model.Unit;
import com.mobile.memhack.model.UnitUser;
import com.mobile.memhack.model.Voca;
import com.mobile.memhack.ui.activity.UnitActivity;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {

    //toolbar
    @BindView(R.id.home_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.rl_course)
    RecyclerView mRlCourse;
    @BindView(R.id.fm_home)
    FrameLayout mFmHome;
    @BindView(R.id.rl_units)
    RecyclerView mRlUnits;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.ll_add_course)
    LinearLayout mLlAddCourse;
    @BindView(R.id.progressbar)
    ProgressBar mProgressBar;
    private ActionBarDrawerToggle mDrawerToggle;

    //data
    private AppManager mManager;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserRef;
    private String idUser;
    private List<Course> mAllCourse;
    private List<CourseUser> mListCourse;
    private CourseUser mCourse;

    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    //context
    Context mContext;

    //adapter
    private RecyclerView.LayoutManager layoutManagerCourse;
    private RecyclerView.LayoutManager layoutManagerUnit;
    private AdapterListCourse mAdapterCourse;
    private AdapterListUnit mAdapterUnit;

    private CallbackVoca callAddVoca = new CallbackVoca() {
        @Override
        public void getListVoca(List<Voca> vocaList) {
            for (int i = 0; i < vocaList.size(); i++) {
                mManager.addVocaUser(vocaList.get(i), idUser);
            }
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private CallbackUnit callAddUnit = new CallbackUnit() {
        @Override
        public void getListUnit(List<Unit> unitList) {
            for (int i = 0; i < unitList.size(); i++) {
                mManager.addUnitUser(unitList.get(i), idUser);
                mManager.callVoca(unitList.get(i).getUnitId(), callAddVoca);
            }
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private AdapterListAllCourse.CallbackChooseCourse callChooseCourse = new AdapterListAllCourse.CallbackChooseCourse() {
        @Override
        public void chooseCourse(Course course) {
            //add course new
            if (isCheckCourse(course)) {
                Toast.makeText(mContext, "Khóa học đã trong danh sách của bạn!", Toast.LENGTH_SHORT).show();
            } else {
                mManager.addCourseUser(course, idUser);
                mManager.callUnit(course.getCourseId(), callAddUnit);
                mManager.callCourseUser(idUser, callbackCourseUser);
                Toast.makeText(mContext, "Thêm khóa học thành công!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private boolean isCheckCourse(Course course) {
        boolean isCourse = false;
        for (int i = 0; i < mListCourse.size(); i++) {
            if (mListCourse.get(i).getCourseId() == course.getCourseId()) {
                isCourse = true;
                break;
            }
        }
        return isCourse;
    }

    private CallbackCourse callAllCourse = new CallbackCourse() {
        @Override
        public void getAllCourse(List<Course> allCourse) {
            mAllCourse.clear();
            mAllCourse = allCourse;
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private CallbackCourseUser callbackCourseUser = new CallbackCourseUser() {
        @Override
        public void getCourseUser(List<CourseUser> courseUsers) {
            mListCourse.clear();
            mListCourse = courseUsers;
            mPref = mContext.getSharedPreferences(BundleUtils.PREF_COURSE_CHOOSE, Activity.MODE_PRIVATE);
            mEditor = mPref.edit();
            if (mPref.getString(BundleUtils.PREF_COURSE_CHOOSE, "").equals("")) {
                mCourse = courseUsers.get(0);
                mToolbar.setTitle(courseUsers.get(0).getCourseName());
                mProgressBar.setMax((int) mCourse.getCourseNumWord());
                mProgressBar.setProgress((int) mCourse.getCourseNumMindWord());
                mEditor.putString(BundleUtils.PREF_COURSE_CHOOSE, String.valueOf(courseUsers.get(0).getCourseId()));
                mEditor.apply();
                mManager.callUnitUser(idUser, courseUsers.get(0).getCourseId(), callUnit);
            } else {
                String idCourse = mPref.getString(BundleUtils.PREF_COURSE_CHOOSE, "");
                Long id = Long.parseLong(idCourse);
                for (int i = 0; i < courseUsers.size(); i++) {
                    if (id.equals(courseUsers.get(i).getCourseId())) {
                        mCourse = courseUsers.get(i);
                        mToolbar.setTitle(courseUsers.get(i).getCourseName());
                        mProgressBar.setMax((int) mCourse.getCourseNumWord());
                        mProgressBar.setProgress((int) mCourse.getCourseNumMindWord());
                    }
                }
                mManager.callUnitUser(idUser, id, callUnit);
            }

            mRlCourse.setLayoutManager(layoutManagerCourse);
            mAdapterCourse = new AdapterListCourse(mContext, mListCourse, chooseCourse);
            mRlCourse.setAdapter(mAdapterCourse);
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };


    private CallbackUnitUser callUnit = new CallbackUnitUser() {
        @Override
        public void getUnitUser(List<UnitUser> unitUsers) {
            mRlUnits.setLayoutManager(layoutManagerUnit);
            mAdapterUnit = new AdapterListUnit(mContext, unitUsers, chooseUnit);
            mRlUnits.setAdapter(mAdapterUnit);
        }

        @Override
        public void notifyError(String message) {
            Log.d("hahaha", "notifyError:" + message);
        }
    };

    private AdapterListCourse.CallbackChooseCourse chooseCourse = new AdapterListCourse.CallbackChooseCourse() {
        @Override
        public void chooseCourse(CourseUser course) {
            mCourse = course;
            mDrawerLayout.closeDrawers();
            mToolbar.setTitle(course.getCourseName());
            mProgressBar.setMax((int) mCourse.getCourseNumWord());
            mProgressBar.setProgress((int) mCourse.getCourseNumMindWord());
            mEditor.putString(BundleUtils.PREF_COURSE_CHOOSE, String.valueOf(course.getCourseId()));
            mEditor.apply();
            mManager.callUnitUser(idUser, course.getCourseId(), callUnit);
        }
    };

    private AdapterListUnit.CallbackChooseUnit chooseUnit = new AdapterListUnit.CallbackChooseUnit() {
        @Override
        public void chooseUnit(UnitUser unit) {
            UnitActivity.start(mContext, unit, mCourse);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = container.getContext();
        initFrag(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void setup(View view) {
        ButterKnife.bind(this, view);
        setView();
    }

    private void setView() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar,
                R.string.menu_open, R.string.menu_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mLlAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mContext);
                LayoutInflater inflater = LayoutInflater.from(mContext);
                View content = inflater.inflate(R.layout.custom_dialog_add_course, null);
                dialog.setContentView(content);
                RecyclerView mRvAllCourse = content.findViewById(R.id.rv_add_course);
                Button mBtnOK = content.findViewById(R.id.btn_ok);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                mRvAllCourse.setLayoutManager(layoutManager);
                AdapterListAllCourse adapter = new AdapterListAllCourse(mContext, mAllCourse, callChooseCourse);
                mRvAllCourse.setAdapter(adapter);
                mBtnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }


    @Override
    public void getData() {
        mAllCourse = new ArrayList<>();
        mListCourse = new ArrayList<>();
        layoutManagerCourse = new LinearLayoutManager(mContext);
        layoutManagerUnit = new LinearLayoutManager(mContext);
        mManager = AppManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            mUserRef = FirebaseDatabase.getInstance().getReference().child("User").child(mAuth.getCurrentUser().getUid());
            idUser = mUserRef.getKey();
            mManager.callAllCourse(callAllCourse);
            mManager.callCourseUser(idUser, callbackCourseUser);
        }
    }

    @Override
    public void updateUI() {

    }

    @Override
    public void releaseData() {

    }
}
