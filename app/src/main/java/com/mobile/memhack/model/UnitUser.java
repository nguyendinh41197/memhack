package com.mobile.memhack.model;

import java.io.Serializable;

public class UnitUser implements Serializable {
    private String id;
    private String idUser;
    private long CourseId;
    private long UnitId;
    private String UnitName;
    private long UnitNumWord;
    private long UnitNumMindWord;

    public UnitUser() {
    }

    public UnitUser(String idUser, long courseId, long unitId, String unitName, long unitNumWord, long unitNumMindWord) {
        this.idUser = idUser;
        CourseId = courseId;
        UnitId = unitId;
        UnitName = unitName;
        UnitNumWord = unitNumWord;
        UnitNumMindWord = unitNumMindWord;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public long getCourseId() {
        return CourseId;
    }

    public void setCourseId(long courseId) {
        CourseId = courseId;
    }

    public long getUnitId() {
        return UnitId;
    }

    public void setUnitId(long unitId) {
        UnitId = unitId;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public long getUnitNumWord() {
        return UnitNumWord;
    }

    public void setUnitNumWord(long unitNumWord) {
        UnitNumWord = unitNumWord;
    }

    public long getUnitNumMindWord() {
        return UnitNumMindWord;
    }

    public void setUnitNumMindWord(long unitNumMindWord) {
        UnitNumMindWord = unitNumMindWord;
    }
}