package com.mobile.memhack.model;

import java.io.Serializable;

public class Voca implements Serializable {
    private long UnitId;
    private String VocaAudio;
    private long VocaId;
    private String VocaMean;
    private String VocaWord;

    public Voca() {
    }

    public long getUnitId() {
        return UnitId;
    }

    public void setUnitId(long unitId) {
        UnitId = unitId;
    }

    public String getVocaAudio() {
        return VocaAudio;
    }

    public void setVocaAudio(String vocaAudio) {
        VocaAudio = vocaAudio;
    }

    public long getVocaId() {
        return VocaId;
    }

    public void setVocaId(long vocaId) {
        VocaId = vocaId;
    }

    public String getVocaMean() {
        return VocaMean;
    }

    public void setVocaMean(String vocaMean) {
        VocaMean = vocaMean;
    }

    public String getVocaWord() {
        return VocaWord;
    }

    public void setVocaWord(String vocaWord) {
        VocaWord = vocaWord;
    }
}