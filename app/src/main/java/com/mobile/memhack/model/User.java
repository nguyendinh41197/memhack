package com.mobile.memhack.model;

import java.io.Serializable;

public class User implements Serializable {
    private String UserId;
    private String UserAvatar;
    private String NumberOfLearned;
    private String Username;
    private String ListFriends;
    private String UserScore;
    private String ListCourseOfUser;

    public User() {
    }

    public User(String userId, String userAvatar, String numberOfLearned, String username, String listFriends, String userScore, String listCourseOfUser) {
        UserId = userId;
        UserAvatar = userAvatar;
        NumberOfLearned = numberOfLearned;
        Username = username;
        ListFriends = listFriends;
        UserScore = userScore;
        ListCourseOfUser = listCourseOfUser;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserAvatar() {
        return UserAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        UserAvatar = userAvatar;
    }

    public String getNumberOfLearned() {
        return NumberOfLearned;
    }

    public void setNumberOfLearned(String numberOfLearned) {
        NumberOfLearned = numberOfLearned;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getListFriends() {
        return ListFriends;
    }

    public void setListFriends(String listFriends) {
        ListFriends = listFriends;
    }

    public String getUserScore() {
        return UserScore;
    }

    public void setUserScore(String userScore) {
        UserScore = userScore;
    }

    public String getListCourseOfUser() {
        return ListCourseOfUser;
    }

    public void setListCourseOfUser(String listCourseOfUser) {
        ListCourseOfUser = listCourseOfUser;
    }
}
