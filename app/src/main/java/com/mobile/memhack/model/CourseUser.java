package com.mobile.memhack.model;

import java.io.Serializable;

public class CourseUser implements Serializable {
    private String id;
    private String idUser;
    private long CourseId;
    private String CourseName;
    private long CourseNumWord;
    private String CourseThumb;
    private long CourseNumMindWord;


    public CourseUser() {
    }

    public CourseUser(String idUser, long courseId, String courseName, long courseNumWord, String courseThumb, long courseNumMindWord) {
        this.idUser = idUser;
        CourseId = courseId;
        CourseName = courseName;
        CourseNumWord = courseNumWord;
        CourseThumb = courseThumb;
        CourseNumMindWord = courseNumMindWord;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public long getCourseId() {
        return CourseId;
    }

    public void setCourseId(long courseId) {
        CourseId = courseId;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public long getCourseNumWord() {
        return CourseNumWord;
    }

    public void setCourseNumWord(long courseNumWord) {
        CourseNumWord = courseNumWord;
    }

    public String getCourseThumb() {
        return CourseThumb;
    }

    public void setCourseThumb(String courseThumb) {
        CourseThumb = courseThumb;
    }

    public long getCourseNumMindWord() {
        return CourseNumMindWord;
    }

    public void setCourseNumMindWord(int courseNumMindWord) {
        CourseNumMindWord = courseNumMindWord;
    }
}