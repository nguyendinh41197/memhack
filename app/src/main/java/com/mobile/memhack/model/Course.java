package com.mobile.memhack.model;

public class Course {
    private long CourseId;
    private String CourseName;
    private long CourseNumWord;
    private String CourseThumb;

    public Course() {
    }

    public long getCourseId() {
        return CourseId;
    }

    public void setCourseId(long courseId) {
        CourseId = courseId;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public long getCourseNumWord() {
        return CourseNumWord;
    }

    public void setCourseNumWord(long courseNumWord) {
        CourseNumWord = courseNumWord;
    }

    public String getCourseThumb() {
        return CourseThumb;
    }

    public void setCourseThumb(String courseThumb) {
        CourseThumb = courseThumb;
    }
}