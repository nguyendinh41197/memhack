package com.mobile.memhack.model;

import java.io.Serializable;

public class VocaUser implements Serializable {
    private String id;
    private long UnitId;
    private String VocaAudio;
    private long VocaId;
    private String VocaMean;
    private long VocaScore;
    private String VocaWord;
    private String idUser;
    private String isBoolde;

    public VocaUser() {
    }

    public VocaUser(long unitId, String vocaAudio, long vocaId, String vocaMean, long vocaScore, String vocaWord, String idUser, String isBoolde) {
        UnitId = unitId;
        VocaAudio = vocaAudio;
        VocaId = vocaId;
        VocaMean = vocaMean;
        VocaScore = vocaScore;
        VocaWord = vocaWord;
        this.idUser = idUser;
        this.isBoolde = isBoolde;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getUnitId() {
        return UnitId;
    }

    public void setUnitId(long unitId) {
        UnitId = unitId;
    }

    public String getVocaAudio() {
        return VocaAudio;
    }

    public void setVocaAudio(String vocaAudio) {
        VocaAudio = vocaAudio;
    }

    public long getVocaId() {
        return VocaId;
    }

    public void setVocaId(long vocaId) {
        VocaId = vocaId;
    }

    public String getVocaMean() {
        return VocaMean;
    }

    public void setVocaMean(String vocaMean) {
        VocaMean = vocaMean;
    }

    public long getVocaScore() {
        return VocaScore;
    }

    public void setVocaScore(long vocaScore) {
        VocaScore = vocaScore;
    }

    public String getVocaWord() {
        return VocaWord;
    }

    public void setVocaWord(String vocaWord) {
        VocaWord = vocaWord;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIsBoolde() {
        return isBoolde;
    }

    public void setIsBoolde(String isBoolde) {
        this.isBoolde = isBoolde;
    }
}