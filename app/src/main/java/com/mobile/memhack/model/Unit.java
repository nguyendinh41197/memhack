package com.mobile.memhack.model;

import java.io.Serializable;

public class Unit implements Serializable {
    private long CourseId;
    private long UnitId;
    private String UnitName;
    private long UnitNumWord;

    public Unit() {
    }

    public long getCourseId() {
        return CourseId;
    }

    public void setCourseId(long courseId) {
        CourseId = courseId;
    }

    public long getUnitId() {
        return UnitId;
    }

    public void setUnitId(long unitId) {
        UnitId = unitId;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public long getUnitNumWord() {
        return UnitNumWord;
    }

    public void setUnitNumWord(long unitNumWord) {
        UnitNumWord = unitNumWord;
    }
}