package com.mobile.memhack.model;

public class Mem {
    private String idMem;
    private long idVoca;
    private String content;
    private String idUser;
    private String nameUser;

    public Mem() {
    }

    public Mem(long idVoca, String content, String idUser, String nameUser) {
        this.idMem = idMem;
        this.idVoca = idVoca;
        this.content = content;
        this.idUser = idUser;
        this.nameUser = nameUser;
    }

    public String getIdMem() {
        return idMem;
    }

    public void setIdMem(String idMem) {
        this.idMem = idMem;
    }

    public long getIdVoca() {
        return idVoca;
    }

    public void setIdVoca(long idVoca) {
        this.idVoca = idVoca;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }
}