package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.memhack.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListWordSp extends RecyclerView.Adapter<AdapterListWordSp.ViewHolder> {
    private Context mContext;
    private List<String> listWord;
    private CallbackChooseWord chooseWord;

    public AdapterListWordSp(Context mContext, List<String> listWord, CallbackChooseWord chooseWord) {
        this.mContext = mContext;
        this.listWord = listWord;
        this.chooseWord = chooseWord;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_text_sp_anwser,viewGroup,false);
        return new ViewHolder(row);    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String word = listWord.get(i);
        viewHolder.mWord.setText(word);
        viewHolder.mWord.setOnClickListener(v -> chooseWord.chooseWord(word));

    }

    @Override
    public int getItemCount() {
        return listWord.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ic_text_sp)
        TextView mWord;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public  interface CallbackChooseWord{
        void chooseWord(String word);
    }
}
