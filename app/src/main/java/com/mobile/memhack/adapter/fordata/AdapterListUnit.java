package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobile.memhack.R;
import com.mobile.memhack.model.UnitUser;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListUnit extends RecyclerView.Adapter<AdapterListUnit.ViewHolser> {

    private Context mContext;
    private List<UnitUser> mListUnit;
    private CallbackChooseUnit chooseUnit;

    public AdapterListUnit(Context context, List<UnitUser> listUnit, CallbackChooseUnit chooseUnit) {
        this.mContext = context;
        this.mListUnit = listUnit;
        this.chooseUnit = chooseUnit;
    }

    @NonNull
    @Override
    public ViewHolser onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_list_unit, viewGroup, false);
        return new ViewHolser(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolser viewHolser, int i) {
        final UnitUser unit = mListUnit.get(i);
        viewHolser.mNameUnit.setText(unit.getUnitName());
        viewHolser.mProgressbar.setMax((int) unit.getUnitNumWord());
        viewHolser.mProgressbar.setProgress((int) unit.getUnitNumMindWord());
        viewHolser.mNumUnit.setText(String.valueOf(unit.getUnitNumWord()));
        if (unit.getUnitNumMindWord()==0){
            viewHolser.mNumMind.setVisibility(View.INVISIBLE);
        }else {
            viewHolser.mNumMind.setVisibility(View.VISIBLE);
            viewHolser.mNumMind.setText(String.valueOf(unit.getUnitNumMindWord()));
        }
        viewHolser.mLlUnit.setOnClickListener(v -> chooseUnit.chooseUnit(unit));
    }

    @Override
    public int getItemCount() {
        return mListUnit.size();
    }

    public class ViewHolser extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item_unit)
        LinearLayout mLlUnit;
        @BindView(R.id.progressBar)
        ProgressBar mProgressbar;
        @BindView(R.id.tv_name_unit)
        TextView mNameUnit;
        @BindView(R.id.txt_numberUnit)
        TextView mNumUnit;
        @BindView(R.id.txt_numberMind)
        TextView mNumMind;

        public ViewHolser(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CallbackChooseUnit {
        void chooseUnit(UnitUser unit);
    }
}
