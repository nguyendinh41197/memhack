package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.model.CourseUser;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListCourse extends RecyclerView.Adapter<AdapterListCourse.ViewHolser> {
    private Context mContext;
    private List<CourseUser> mListCourse;
    private CallbackChooseCourse callbackchoose;

    public AdapterListCourse(Context context, List<CourseUser> listCourse, CallbackChooseCourse callbackchoose) {
        this.mContext = context;
        this.mListCourse = listCourse;
        this.callbackchoose = callbackchoose;
    }

    @NonNull
    @Override
    public ViewHolser onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_list_course, viewGroup, false);
        return new ViewHolser(row);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolser viewHolser, int i) {
        final CourseUser course = mListCourse.get(i);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final  StorageReference imgRef = storageReference.child("image/" + course.getCourseThumb());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(mContext).load(uri).into(viewHolser.mThumbCourse);
            }
        });
        viewHolser.mNameCourse.setText(course.getCourseName());
        viewHolser.mNumberWord.setText(course.getCourseNumMindWord() + "/" + course.getCourseNumWord());
        viewHolser.mProgressBar.setMax((int) course.getCourseNumWord());
        viewHolser.mProgressBar.setProgress((int) course.getCourseNumMindWord());
        viewHolser.mLlCourse.setOnClickListener(v  -> callbackchoose.chooseCourse(course));
    }

    @Override
    public int getItemCount() {
        return mListCourse.size();
    }

    public class ViewHolser extends RecyclerView.ViewHolder{

        @BindView(R.id.ll_item_course)
        LinearLayout mLlCourse;
        @BindView(R.id.img_thumb_course)
        ImageView mThumbCourse;
        @BindView(R.id.tv_name_course)
        TextView mNameCourse;
        @BindView(R.id.tv_number_word)
        TextView mNumberWord;
        @BindView(R.id.progressbar)
        ProgressBar mProgressBar;

        public ViewHolser(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CallbackChooseCourse{
        void chooseCourse(CourseUser course);
    }
}
