package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.model.Course;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListAllCourse extends RecyclerView.Adapter<AdapterListAllCourse.ViewHolser> {
    private Context mContext;
    private List<Course> mListCourse;
    private CallbackChooseCourse chooseCourse;

    public AdapterListAllCourse(Context mContext, List<Course> mListCourse, CallbackChooseCourse chooseCourse) {
        this.mContext = mContext;
        this.mListCourse = mListCourse;
        this.chooseCourse = chooseCourse;
    }

    @NonNull
    @Override
    public AdapterListAllCourse.ViewHolser onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_list_all_course, viewGroup, false);
        return new ViewHolser(row);    }

    @Override
    public void onBindViewHolder(@NonNull AdapterListAllCourse.ViewHolser viewHolser, int i) {
        Course course = mListCourse.get(i);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final  StorageReference imgRef = storageReference.child("image/" + course.getCourseThumb());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(mContext).load(uri).into(viewHolser.mImgThumb);
            }
        });
        viewHolser.mTxtName.setText(course.getCourseName());
        viewHolser.mRlAdd.setOnClickListener(v -> chooseCourse.chooseCourse(course));
    }

    @Override
    public int getItemCount() {
        return mListCourse.size();
    }

    public class ViewHolser extends RecyclerView.ViewHolder {
        @BindView(R.id.img_thumb_course)
        ImageView mImgThumb;
        @BindView(R.id.txt_name_course)
        TextView mTxtName;
        @BindView(R.id.rl_add)
        RelativeLayout mRlAdd;

        public ViewHolser(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CallbackChooseCourse{
        void chooseCourse(Course course);
    }
}
