package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterListFriend extends RecyclerView.Adapter<AdapterListFriend.ViewHoder> {
    private List<User> userList;
    private Context mContext;

    public AdapterListFriend(List<User> userList, Context mContext) {
        this.userList = userList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_list_friend, viewGroup, false);
        return new ViewHoder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder viewHoder, int i) {
        User user = userList.get(i);
        viewHoder.mTxtStt.setText(i + 1 + "");
        viewHoder.mTxtName.setText(user.getUsername());
        viewHoder.mTxtScore.setText(user.getUserScore());
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final StorageReference imgRef = storageReference.child("avatar/" + user.getUserAvatar());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(mContext).load(uri).into(viewHoder.mAvata);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHoder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_stt)
        TextView mTxtStt;
        @BindView(R.id.avata)
        CircleImageView mAvata;
        @BindView(R.id.txt_name_user)
        TextView mTxtName;
        @BindView(R.id.txt_score)
        TextView mTxtScore;

        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
