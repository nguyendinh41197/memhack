package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListFriendSearch extends RecyclerView.Adapter<AdapterListFriendSearch.ViewHolder> {
    private Context mContext;
    private List<User> userList;
    private CallbackChooseAddFriend addFriend;

    public AdapterListFriendSearch(Context mContext, List<User> userList, CallbackChooseAddFriend addFriend) {
        this.mContext = mContext;
        this.userList = userList;
        this.addFriend = addFriend;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_friend_search, viewGroup, false);
        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        User user = userList.get(i);
        viewHolder.mName.setText(user.getUsername());
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final StorageReference imgRef = storageReference.child("avatar/" + user.getUserAvatar());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(mContext).load(uri).into(viewHolder.icAvata);
            }
        });
        viewHolder.mAddFriend.setOnClickListener(v -> addFriend.chooseAddFriend(user));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avata)
        ImageView icAvata;
        @BindView(R.id.txt_name_user)
        TextView mName;
        @BindView(R.id.ic_add_friend)
        ImageView mAddFriend;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CallbackChooseAddFriend{
        void chooseAddFriend(User user);
    }
}
