package com.mobile.memhack.adapter.forview;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.fordata.AdapterListWordSp;
import com.mobile.memhack.data.AppManager;
import com.mobile.memhack.data.callback.CallbackVocaUser;
import com.mobile.memhack.model.CourseUser;
import com.mobile.memhack.model.Question;
import com.mobile.memhack.model.UnitUser;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.VocaUser;
import com.mobile.memhack.ui.utils.BundleUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

public class AdapterViewLearn extends PagerAdapter {
    private List<Question> questionList;
    private List<VocaUser> vocaUsers;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private User user;
    private CallbackNextpage nextpage;
    private SharedPreferences mPrefCourse;
    private SharedPreferences mPrefUnit;

    public AdapterViewLearn(List<Question> questionList, List<VocaUser> vocaUsers, Context mContext, User user, CallbackNextpage nextpage) {
        this.questionList = questionList;
        this.vocaUsers = vocaUsers;
        this.mContext = mContext;
        this.user = user;
        this.nextpage = nextpage;
    }

    @Override
    public int getCount() {
        return questionList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = LayoutInflater.from(mContext);
        View view1 = layoutInflater.inflate(R.layout.item_question_type_1, container, false);
        View view2 = layoutInflater.inflate(R.layout.item_question_type_2, container, false);
        Question question = questionList.get(position);

        mPrefCourse = mContext.getSharedPreferences(BundleUtils.KEY_SEND_ID_COURSE, Activity.MODE_PRIVATE);
        mPrefUnit = mContext.getSharedPreferences(BundleUtils.KEY_SEND_ID_UNIT, Activity.MODE_PRIVATE);

        VocaUser vocaUser = new VocaUser();

        for (int i = 0; i < vocaUsers.size(); i++) {
            if (question.getIdVoca() == vocaUsers.get(i).getVocaId()) {
                vocaUser = vocaUsers.get(i);
                break;
            }
        }

        //setup view 1
        TextView mVocaVNv1 = view1.findViewById(R.id.txt_voca);
        ProgressBar mProgressBarv1 = view1.findViewById(R.id.progressBar);
        mProgressBarv1.setVisibility(View.INVISIBLE);
        TextView mAnwserA = view1.findViewById(R.id.answerA);
        TextView mAnwserB = view1.findViewById(R.id.answerB);
        TextView mAnwserC = view1.findViewById(R.id.answerC);
        TextView mAnwserD = view1.findViewById(R.id.answerD);

        mVocaVNv1.setText(vocaUser.getVocaMean());
        mProgressBarv1.setMax(10);
        mProgressBarv1.setProgress((int) vocaUser.getVocaScore());
        mAnwserA.setText(question.getAnswerA());
        mAnwserB.setText(question.getAnswerB());
        mAnwserC.setText(question.getAnswerC());
        mAnwserD.setText(question.getAnswerD());

        VocaUser finalVocaUser = vocaUser;
        mAnwserA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((question.getAnswerA()).equals(finalVocaUser.getVocaWord())) {
                    mAnwserA.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    plusPoints(finalVocaUser);
                } else {
                    mAnwserA.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_answer_false));
                    minusPoints(finalVocaUser);
                    if ((question.getAnswerB()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserB.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerC()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserC.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerD()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserD.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    }
                }

                //mProgressBarv1.setProgress((int)(updateUI(finalVoca).getVocaScore()));

                delayTime();
            }
        });
        mAnwserB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((question.getAnswerB()).equals(finalVocaUser.getVocaWord())) {
                    mAnwserB.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    plusPoints(finalVocaUser);
                } else {
                    mAnwserB.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_answer_false));
                    minusPoints(finalVocaUser);
                    if ((question.getAnswerA()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserA.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerC()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserC.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerD()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserD.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    }
                }
                //mProgressBarv1.setProgress((int)(updateUI(finalVoca).getVocaScore()));

                delayTime();
            }
        });
        mAnwserC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((question.getAnswerC()).equals(finalVocaUser.getVocaWord())) {
                    mAnwserC.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    plusPoints(finalVocaUser);
                } else {
                    mAnwserC.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_answer_false));
                    minusPoints(finalVocaUser);
                    if ((question.getAnswerA()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserA.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerB()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserB.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerD()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserD.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    }
                }
                //mProgressBarv1.setProgress((int)(updateUI(finalVoca).getVocaScore()));

                delayTime();
            }
        });
        mAnwserD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((question.getAnswerD()).equals(finalVocaUser.getVocaWord())) {
                    mAnwserD.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    plusPoints(finalVocaUser);
                } else {
                    mAnwserD.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_answer_false));
                    minusPoints(finalVocaUser);
                    if ((question.getAnswerA()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserA.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerB()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserB.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    } else if ((question.getAnswerC()).equals(finalVocaUser.getVocaWord())) {
                        mAnwserC.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    }
                }
                //mProgressBarv1.setProgress((int)(updateUI(finalVoca).getVocaScore()));

                delayTime();
            }
        });


        //setup view 2
        TextView mVocaVNv2 = view2.findViewById(R.id.txt_voca);
        ProgressBar mProgressBarv2 = view2.findViewById(R.id.progressBar);
        mProgressBarv2.setVisibility(View.INVISIBLE);
        EditText mEdtAnswer = view2.findViewById(R.id.edt_answer);
        RecyclerView mRvSp = view2.findViewById(R.id.rv_sp);
        Button mNext = view2.findViewById(R.id.btn_next);

        mVocaVNv2.setText(vocaUser.getVocaMean());
        mProgressBarv2.setMax(10);
        mProgressBarv2.setProgress((int) vocaUser.getVocaScore());

        mEdtAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    mNext.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_answer_type2));
                    mNext.setText("Tiếp theo");
                } else {
                    mNext.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_gray));
                    mNext.setText("Bỏ qua");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //set view list sp
        char[] s = vocaUser.getVocaWord().replaceAll("", "").toCharArray();

        List<String> strings = new ArrayList<>();
        for (char w : s) {
            strings.add(String.valueOf(w));
        }

        List<String> listAlphabet = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        List<String> listRandom = getRandomElement(listAlphabet, 3);

        for (int i = 0; i < listRandom.size(); i++) {
            strings.add(listRandom.get(i));
        }
        Collections.shuffle(strings);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 7);
        mRvSp.setLayoutManager(mLayoutManager);
        AdapterListWordSp adapter = new AdapterListWordSp(mContext, strings, new AdapterListWordSp.CallbackChooseWord() {
            @Override
            public void chooseWord(String word) {
                String anwser = mEdtAnswer.getText().toString();
                mEdtAnswer.setText(anwser + word);
            }
        });
        mRvSp.setAdapter(adapter);

        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((mEdtAnswer.getText().toString()).equals(finalVocaUser.getVocaWord().toString())) {
                    mNext.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_true));
                    mNext.setText("Chính xác!");
                    plusPoints(finalVocaUser);
                } else {
                    mNext.setBackground(mContext.getResources().getDrawable(R.drawable.circle_button_answer_false));
                    mNext.setText("Sai rồi!");
                    minusPoints(finalVocaUser);
                }

                //mProgressBarv1.setProgress((int)(updateUI(finalVoca).getVocaScore()));
                delayTime();
            }
        });

        //sap xep cau hoi
        SharedPreferences check = mContext.getSharedPreferences(BundleUtils.KEY_RANDOM_QUESTION, MODE_PRIVATE);
        SharedPreferences.Editor editor = check.edit();

        if (check.getString(BundleUtils.KEY_RANDOM_QUESTION, "").equals("")) {
            editor.putString(BundleUtils.KEY_RANDOM_QUESTION, "type2");
            editor.commit();
        }

        if (check.getString(BundleUtils.KEY_RANDOM_QUESTION, "").equals("type2")) {
            editor.putString(BundleUtils.KEY_RANDOM_QUESTION, "type1");
            editor.commit();
            container.addView(view1, 0);
            return view1;
        } else if (check.getString(BundleUtils.KEY_RANDOM_QUESTION, "").equals("type1")) {
            editor.putString(BundleUtils.KEY_RANDOM_QUESTION, "type2");
            editor.commit();
            container.addView(view2, 0);
            return view2;
        }

        return view1;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public List<String> getRandomElement(List<String> list, int totalItems) {
        Random rand = new Random();
        List<String> newList = new ArrayList<>();
        for (int i = 0; i < totalItems; i++) {
            int randomIndex = rand.nextInt(list.size());
            newList.add(list.get(randomIndex));
        }
        return newList;
    }

    public interface CallbackNextpage {
        void nextPage();
    }

    private void plusPoints(VocaUser vocaUser) {
        DatabaseReference mUserRef = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUserId());
        DatabaseReference mVocaRef = FirebaseDatabase.getInstance().getReference().child("VocaUser").child(String.valueOf(vocaUser.getId()));
        DatabaseReference mUnitRef = FirebaseDatabase.getInstance().getReference().child("UnitUser").child(String.valueOf(mPrefUnit.getString(BundleUtils.KEY_SEND_ID_UNIT, "")));
        DatabaseReference mCourseRef = FirebaseDatabase.getInstance().getReference().child("CourseUser").child(String.valueOf(mPrefCourse.getString(BundleUtils.KEY_SEND_ID_COURSE, "")));

        //plus points of voca
        long scoreVoca = vocaUser.getVocaScore() + 3;
        mVocaRef.child("vocaScore").setValue(scoreVoca);

        //plus points of user
        int scoreUser = Integer.parseInt(user.getUserScore()) + 45;
        Log.e("TAG", "1234:" + scoreUser);
        mUserRef.child("UserScore").setValue(String.valueOf(scoreUser));

        //plus unit and course
        if (scoreVoca >= 10) {
            int numWordUser = Integer.parseInt(user.getNumberOfLearned()) + 1;
            long[] numWordUnit = {0};
            mUnitRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    UnitUser unitUser = dataSnapshot.getValue(UnitUser.class);
                    numWordUnit[0] = unitUser.getUnitNumMindWord();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            long[] numWordCourse = {0};
            mCourseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    CourseUser courseUser = dataSnapshot.getValue(CourseUser.class);
                    numWordCourse[0] = courseUser.getCourseNumMindWord();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            try {
                mVocaRef.child("isBoolde").setValue("true");
                mUserRef.child("NumberOfLearned").setValue(String.valueOf(numWordUser));
                mUnitRef.child("unitNumMindWord").setValue(numWordUnit[0] + 1);
                mCourseRef.child("courseNumMindWord").setValue(numWordCourse[0] + 1);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        AppManager mManager = AppManager.getInstance();
        mManager.callVocaUser(user.getUserId(), vocaUser.getUnitId(), new CallbackVocaUser() {
            @Override
            public void getVocaUser(List<VocaUser> list) {
                vocaUsers = list;
            }

            @Override
            public void notifyError(String message) {

            }
        });



    }

    private void minusPoints(VocaUser vocaUser) {
        //minus points of voca
        DatabaseReference mUserRef = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUserId());
        DatabaseReference mVocaRef = FirebaseDatabase.getInstance().getReference().child("VocaUser").child(String.valueOf(vocaUser.getId()));
        DatabaseReference mUnitRef = FirebaseDatabase.getInstance().getReference().child("UnitUser").child(String.valueOf(mPrefUnit.getString(BundleUtils.KEY_SEND_ID_UNIT, "")));
        DatabaseReference mCourseRef = FirebaseDatabase.getInstance().getReference().child("CourseUser").child(String.valueOf(mPrefCourse.getString(BundleUtils.KEY_SEND_ID_COURSE, "")));

        if (vocaUser.getVocaScore() > 0) {
            long scoreVoca = vocaUser.getVocaScore() - 1;
            mVocaRef.child("vocaScore").setValue(scoreVoca);

            if (scoreVoca == 0) {
                long[] numWordUnit = {0};
                mUnitRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        UnitUser unitUser = dataSnapshot.getValue(UnitUser.class);
                        numWordUnit[0] = unitUser.getUnitNumMindWord();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                long[] numWordCourse = {0};
                mCourseRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        CourseUser courseUser = dataSnapshot.getValue(CourseUser.class);
                        numWordCourse[0] = courseUser.getCourseNumMindWord();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                try {
                    mVocaRef.child("isBoolde").setValue("false");
                    mUnitRef.child("unitNumMindWord").setValue(numWordUnit[0] - 1);
                    mCourseRef.child("courseNumMindWord").setValue(numWordCourse[0] - 1);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //minus points of user
        if (Integer.parseInt(user.getUserScore()) > 6) {
            int scoreUser = Integer.parseInt(user.getUserScore()) - 7;
            mUserRef.child("UserScore").setValue(String.valueOf(scoreUser));

            if ((vocaUser.getVocaScore() - 1) == 0) {
                int numWord = Integer.parseInt(user.getNumberOfLearned()) - 1;
                try {
                    mUserRef.child("NumberOfLearned").setValue(String.valueOf(numWord));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }

        mUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        AppManager mManager = AppManager.getInstance();
        mManager.callVocaUser(user.getUserId(), vocaUser.getUnitId(), new CallbackVocaUser() {
            @Override
            public void getVocaUser(List<VocaUser> list) {
                vocaUsers = list;
            }

            @Override
            public void notifyError(String message) {

            }
        });

    }

    private void delayTime() {
        new CountDownTimer(1080, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                nextpage.nextPage();
            }
        }.start();
    }

    private VocaUser updateUI(VocaUser voca) {
        final VocaUser[] vocaUser = {new VocaUser()};
        AppManager mManager = AppManager.getInstance();
        mManager.callVocaUser(voca.getIdUser(), voca.getUnitId(), new CallbackVocaUser() {
            @Override
            public void getVocaUser(List<VocaUser> vocaUsers) {
                for (int i = 0; i < vocaUsers.size(); i++) {
                    if (voca.getId().equals(vocaUsers.get(i).getId())) {
                        vocaUser[0] = vocaUsers.get(i);
                    }
                }
            }

            @Override
            public void notifyError(String message) {

            }
        });

        return vocaUser[0];
    }
}
