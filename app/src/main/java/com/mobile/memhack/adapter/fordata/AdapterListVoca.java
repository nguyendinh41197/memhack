package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobile.memhack.R;
import com.mobile.memhack.model.VocaUser;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListVoca extends RecyclerView.Adapter<AdapterListVoca.ViewHoder> {
    private Context mContext;
    private List<VocaUser> mListVoca;
    private CallbackChooseVoca callbackChooseVoca;

    public AdapterListVoca(Context mContext, List<VocaUser> mListVoca, CallbackChooseVoca callbackChooseVoca) {
        this.mContext = mContext;
        this.mListVoca = mListVoca;
        this.callbackChooseVoca = callbackChooseVoca;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_list_voca,viewGroup,false);
        return new ViewHoder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder viewHoder, int i) {
        final  VocaUser voca = mListVoca.get(i);
        viewHoder.txtWordEN.setText(voca.getVocaWord());
        viewHoder.txtWordVN.setText(voca.getVocaMean());
        viewHoder.mThumbVoca.setImageResource(R.drawable.learn);
        viewHoder.mLlVoca.setOnClickListener(v -> callbackChooseVoca.chooseVoca(voca, i));
        if (voca.getIsBoolde().equals("true")){
            viewHoder.mThumbRate.setBackground(mContext.getResources().getDrawable(R.drawable.ic_star_y_24dp));
        }else {
            viewHoder.mThumbRate.setBackground(mContext.getResources().getDrawable(R.drawable.ic_star_g_24dp));
        }
    }

    @Override
    public int getItemCount() {
        return mListVoca.size();
    }

    public class ViewHoder extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item_voca)
        LinearLayout mLlVoca;
        @BindView(R.id.img_voca)
        ImageView mThumbVoca;
        @BindView(R.id.img_rate)
        ImageView mThumbRate;
        @BindView(R.id.txt_word_en)
        TextView txtWordEN;
        @BindView(R.id.txt_word_vn)
        TextView txtWordVN;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
    public  interface CallbackChooseVoca{
        void chooseVoca(VocaUser voca, int i);
    }
}
