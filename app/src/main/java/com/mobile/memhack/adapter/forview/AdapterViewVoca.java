package com.mobile.memhack.adapter.forview;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mobile.memhack.R;
import com.mobile.memhack.adapter.fordata.AdapterListMem;
import com.mobile.memhack.model.Mem;
import com.mobile.memhack.model.User;
import com.mobile.memhack.model.VocaUser;
import com.mobile.memhack.ui.activity.AddMemActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdapterViewVoca extends PagerAdapter {
    private List<VocaUser> vocaList;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private int position;
    private User user;

    public AdapterViewVoca(List<VocaUser> vocaList, Context mContext, int position, User user) {
        this.vocaList = vocaList;
        this.mContext = mContext;
        this.position = position;
        this.user = user;
    }

    @Override
    public int getCount() {
        return vocaList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int i) {
        layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.item_detail_voca, container, false);
        VocaUser voca = vocaList.get(i);

        TextView txtWordEn = view.findViewById(R.id.txt_word_en);
        TextView txtWordVn = view.findViewById(R.id.txt_word_vn);
        ImageView icAudio = view.findViewById(R.id.ic_audio);
        LinearLayout llIsMem = view.findViewById(R.id.ll_isMem);
        LinearLayout llNoMem = view.findViewById(R.id.ll_noMem);
        RecyclerView rvMem = view.findViewById(R.id.rv_mem);
        Button btnAddMem = view.findViewById(R.id.btn_add_mem);
        Button btnCreateMem = view.findViewById(R.id.btn_create_mem);

        final List<Mem> mMemList = new ArrayList<>();
        List<Mem> mems = new ArrayList<>();

        DatabaseReference mDataRefMem = FirebaseDatabase.getInstance().getReference().child("Mem");
        mDataRefMem.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mems.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Mem mem = snapshot.getValue(Mem.class);
                    if (mem.getIdVoca() == voca.getVocaId()) {
                        mems.add(mem);
                    }
                }

                String[] friends = user.getListFriends().split(",");

                mMemList.clear();
                // sap xep thu tu theo diem
                for (int i = 0; i < mems.size(); i++) {
                    for (String w : friends) {
                        if (w.equals(mems.get(i).getIdUser())) {
                            mMemList.add(mems.get(i));
                        }
                    }
                }

                if (mMemList.size() != 0) {
                    llIsMem.setVisibility(View.VISIBLE);
                    llNoMem.setVisibility(View.INVISIBLE);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
                    rvMem.setLayoutManager(layoutManager);
                    AdapterListMem adapterListMem = new AdapterListMem(mContext, mMemList);
                    rvMem.setAdapter(adapterListMem);
                } else {
                    llIsMem.setVisibility(View.INVISIBLE);
                    llNoMem.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        final String[] url = new String[1];

        txtWordEn.setText(voca.getVocaWord());
        txtWordVn.setText(voca.getVocaMean());

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final StorageReference imgRef = storageReference.child("audio/" + voca.getVocaAudio());
        imgRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                url[0] = uri.toString();
            }
        });

        icAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                icAudio.setBackground(mContext.getResources().getDrawable(R.drawable.ic_un_audio_24dp));
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(url[0]);
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                    mediaPlayer.prepare();
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            icAudio.setBackground(mContext.getResources().getDrawable(R.drawable.ic_audio_24dp));
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });
        btnAddMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMemActivity.start(mContext, voca, user);
            }
        });
        btnCreateMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMemActivity.start(mContext, voca, user);
            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}

