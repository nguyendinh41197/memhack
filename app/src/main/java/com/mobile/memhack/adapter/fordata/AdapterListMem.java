package com.mobile.memhack.adapter.fordata;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobile.memhack.R;
import com.mobile.memhack.model.Mem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListMem extends RecyclerView.Adapter<AdapterListMem.ViewHolser> {
    private Context mContext;
    private List<Mem> memList;

    public AdapterListMem(Context mContext, List<Mem> memList) {
        this.mContext = mContext;
        this.memList = memList;
    }

    @NonNull
    @Override
    public ViewHolser onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_list_mem, viewGroup, false);
        return new ViewHolser(row);    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolser viewHolser, int i) {
        Mem mem = memList.get(i);
        viewHolser.mName.setText(mem.getNameUser());
        viewHolser.mMem.setText(mem.getContent());
    }

    @Override
    public int getItemCount() {
        return memList.size();
    }

    public class ViewHolser extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_user)
        TextView mName;
        @BindView(R.id.txt_mem)
        TextView mMem;

        public ViewHolser(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
